angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel', 'ui.router', 'ngScrollbars'])

        .config(function (ScrollBarsProvider) {
            // scrollbar defaults
            ScrollBarsProvider.defaults = {
                autoHideScrollbar: false,
                setHeight: 400,
                scrollInertia: 0,
                axis: 'y',
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: true
                }
            };
        })
   
   
   .controller('AppCtrl', function ($scope,$mdSidenav,$presence,$chat,$auth) {
   
		$scope.scrollbarConfig = {
                theme: 'minimal-dark',
                scrollInertia: 500
        }
   
		$scope.selectedUser = "Duo Connect";
		$scope.currentUser = "Log in";
   
		function buildToggler(navID) {
		  return function() {
			return $mdSidenav(navID).toggle()
			  .then(function () {
				$log.debug("toggle " + navID + " is done");
			  });
		  }
		}
		
		//TEMPORARY FORCE-LOGINS START
	    $scope.login1 = function(){
	   
			$auth.onLoginResult(function(){
				$scope.currentUser = $auth.getUserName();
				$presence.setOnline();
			});
	
			$auth.forceLogin("Dilshan","user1","123");
			chat();
	   }
   
		$scope.login2 = function(){
			$auth.onLoginResult(function()
			{
				$scope.currentUser = $auth.getUserName();
				$presence.setOnline();
			});
		
			$auth.forceLogin("Hiflan","user2","1234");
		
			chat();
   
		}
	
		$scope.login3 = function(){
	   
			$auth.onLoginResult(function(){
				$scope.currentUser = $auth.getUserName();
				$presence.setOnline();
			});
		
			$auth.forceLogin("Supun","user1","1235");
			chat();
	   }
   
	    $scope.login4 = function(){
			$auth.onLoginResult(function()
			{
				$scope.currentUser = $auth.getUserName();
				$presence.setOnline();
			});
		
			$auth.forceLogin("Eshwaran","user2","1236");
			chat();
		}
		//TEMPORARY FORCE-LOGINS FINISH
	
	
		var diableChat = true;
	    $scope.diableChatInput = function()
	    {	
			 return diableChat;
	    }
   
		function chat() {
	   
			//location.href = '#/chat';
			$scope.loginButton = false;
			$scope.currentMessage = "";

			$scope.allUsers = [];

			var messageId=0;
			
			
			$scope.selectUser = function(user){
			
				$scope.selectedUser = user.userName;
				diableChat = false;
				$scope.diableChatInput();
				
			}
			
			
			$scope.sendMessage = function(keyEvent){
				
				//Get user input for chat with enter key if the input is not empty
				if (keyEvent.which === 13)
					{
						if($scope.currentMessage)
						{
							$chat.send($scope.selectedUser, $scope.currentUser, $scope.currentMessage);
							//$scope.receivingMessages.push({mId: messageId++, message:$scope.currentMessage, sender: $scope.currentUser});
							$( "#chatContentLarge" ).append( "<img src='img/profilepic.png' width='40px' height='40px' style='border-radius:100px;float:right;margin-top:10px;margin-right:5px;'><md-card class='md-card-right' style='background:white;float:right'><md-card-content>"+$scope.currentMessage+"</md-card-content></md-card> <br/><br/><br/><br/>" );
							$scope.currentMessage = "";
						}
					}
			}

			$presence.onUserStateChanged(function(e,data){
				if (data.state == "online"){
					$scope.allUsers.push({userName:data.userName});
				} else{
					var removeIndex =-1;
					for (index in $scope.allUsers)
					if ($scope.allUsers[index].userName==data.userName){
						removeIndex = index;
						break;
					}

					if (removeIndex!=-1)
						$scope.allUsers.splice(removeIndex,1);
				}
			});

			$presence.onStateChanged(function(e,data){
				$presence.getOnlineUsers();
			});

			$chat.onMessage(function(e,data){
				//$scope.receivingMessages.push({mId: messageId++, message:data.message, sender:$scope.toUser});
				$( "#chatContentLarge" ).append("<div style='float:left'><img src='img/caller.jpg' width='40px' height='40px' style='border-radius:100px;margin-top:10px;margin-left:5px;'><md-card class='md-card-left' style='background:white;float:right'><md-card-content>"+data.message+"</md-card-content></md-card></div><br/><br/><br/><br/> ");
			});
			
			$presence.onOnlineUsersLoaded(function(e,data){
				var newUsers  = [];
				var cUser = $auth.getUserName();
				for (var uIndex in data.users)
					if (data.users[uIndex].userName != $scope.currentUser)
						newUsers.push(data.users[uIndex]);
						
				$scope.allUsers = newUsers;
			});
		
	    }
   
   })//END OF AppCtrl
   
    .controller('DialCtrl', function($scope) {
		var currentNumber;
		$scope.changeNumber = function (num) {
			
			if(currentNumber == null || currentNumber == "")
			{
				currentNumber = num;
				$scope.dialingNumber = currentNumber;
			}else
			{
				currentNumber = currentNumber+num;
				$scope.dialingNumber = currentNumber;
			}
			
		}
		$scope.backspace = function() {
			currentNumber = currentNumber.substring(0, currentNumber.length - 1);
			$scope.dialingNumber = currentNumber;
		}
	})//END OF DialCtrl

	
	//REFER TO THE DIRECTIVE LIBRARY DOCUMENTATION FOR BELOW CODE
	.controller('floatingButtonControl', function Ctrl(mfbDefaultValues, $window){
		var vm = this;
		
		vm.positions = mfbDefaultValues.positions;
		vm.effects = mfbDefaultValues.effects;
		vm.methods = mfbDefaultValues.methods;
		vm.actions = mfbDefaultValues.actions;

		vm.menuState = 'closed';
		vm.loc = loc;
		vm.setMainAction = setMainAction;
		vm.mainAction = mainAction;

		vm.chosen = {
			effect : 'zoomin',
			position : 'br',
			method : 'click',
			action : 'fire'
		};

		vm.buttons = [{
			label: 'Video call',
			icon: 'ion-ios-videocam',
			href: 'videoCall'
		},{
			label: 'Call',
			icon: 'ion-ios-telephone',
			href: 'call'
		},{
			label: 'Add people',
			icon: 'ion-plus-round',
			href: 'addPeople'
		}];

		function loc(href) {
			vm.menuState = 'closed'; 	
				
			if(href == 'videoCall')
			{
				location.href = '#/videochat';
			}
			else if(href == 'call')
			  call();
			else if(href == 'addPeople')
			  addPeople();  
		}
		
		function videoCall()
		{
			alert("Video Call");		
		}
		function call()
		{
			alert("Call");
		}
		function addPeople()
		{
			alert("Add people");		
		}
		
		function mainAction() {
		  //console.log('Firing Main Action!');
		}

		function setMainAction() {
			if(vm.chosen.action === 'fire') {
				vm.mainAction = mainAction;
			} else {
				vm.mainAction = null;
			}
		}
	})//END OF floatingButtonControl
	  
  
   .config(function($stateProvider, $urlRouterProvider) {
    
		$urlRouterProvider.otherwise('/chat');
    
		$stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
       
		 .state('home', {
            url: '/home',
            templateUrl: 'partials/welcome.html'
			
        })
		
		.state('videochat', {
				url: '/videochat',
				templateUrl: 'partials/webcam.html',
				controller: function($scope) {
					$scope.disableMiniChat = false;
				}
			})
         .state('chat', {
            url: '/chat',
            templateUrl: 'partials/Chat.html'
			
        })
	})
	
	 .controller('BasicDemoCtrl', DemoCtrl);
  function DemoCtrl ($timeout, $q) {
    var self = this;
    self.readonly = false;
    // Lists of fruit names and Vegetable objects
    self.fruitNames = ['Apple', 'Banana', 'Orange'];
    self.roFruitNames = angular.copy(self.fruitNames);
    self.tags = [];
    self.vegObjs = [
      {
        'name' : 'Broccoli',
        'type' : 'Brassica'
      },
      {
        'name' : 'Cabbage',
        'type' : 'Brassica'
      },
      {
        'name' : 'Carrot',
        'type' : 'Umbelliferous'
      }
    ];
    self.newVeg = function(chip) {
      return {
        name: chip,
        type: 'unknown'
      };
    };
  }
  

  
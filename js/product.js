angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel', 'ui.router', 'ngScrollbars'])

	
        
   
   
   .controller('AppCtrl', function ($scope) {
   
		$scope.scrollbarConfig = {
			autoHideScrollbar: false,
			theme: 'minimal-dark',
			axis: 'y',
			advanced: {
			updateOnContentResize: true
			},
			scrollInertia: 300
		}
   
		$scope.hideProductDetails = function()
		{
			alert("what");
		}
		
		
		
	
	
		
   
   })//END OF AppCtrl
   
    

	
	//REFER TO THE DIRECTIVE LIBRARY DOCUMENTATION FOR BELOW CODE
	.controller('floatingButtonControl', function Ctrl(mfbDefaultValues, $window){
		var vm = this;
		
		vm.positions = mfbDefaultValues.positions;
		vm.effects = mfbDefaultValues.effects;
		vm.methods = mfbDefaultValues.methods;
		vm.actions = mfbDefaultValues.actions;

		vm.menuState = 'closed';
		vm.loc = loc;
		vm.setMainAction = setMainAction;
		vm.mainAction = mainAction;

		vm.chosen = {
			effect : 'zoomin',
			position : 'br',
			method : 'click',
			action : 'fire'
		};

		vm.buttons = [{
			label: 'Video call',
			icon: 'ion-ios-videocam',
			href: 'videoCall'
		},{
			label: 'Call',
			icon: 'ion-ios-telephone',
			href: 'call'
		},{
			label: 'Add people',
			icon: 'ion-plus-round',
			href: 'addPeople'
		}];

		function loc(href) {
			vm.menuState = 'closed'; 	
				
			if(href == 'videoCall')
			{
				location.href = '#/videochat';
			}
			else if(href == 'call')
			  call();
			else if(href == 'addPeople')
			  addPeople();  
		}
		
		function videoCall()
		{
			alert("Video Call");		
		}
		function call()
		{
			alert("Call");
		}
		function addPeople()
		{
			alert("Add people");		
		}
		
		function mainAction() {
		  //console.log('Firing Main Action!');
		}

		function setMainAction() {
			if(vm.chosen.action === 'fire') {
				vm.mainAction = mainAction;
			} else {
				vm.mainAction = null;
			}
		}
	})//END OF floatingButtonControl
	  
  
   .config(function($stateProvider, $urlRouterProvider) {
    
		$urlRouterProvider.otherwise('/chat');
    
		$stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
       
		 .state('home', {
            url: '/home',
            templateUrl: 'partials/welcome.html'
			
        })
		
		.state('videochat', {
				url: '/videochat',
				templateUrl: 'partials/webcam.html',
				controller: function($scope) {
					$scope.disableMiniChat = false;
				}
			})
         .state('chat', {
            url: '/chat',
            templateUrl: 'partials/Chat.html'
			
        })
	})
	
	 .controller('BasicDemoCtrl', DemoCtrl);
  function DemoCtrl ($timeout, $q) {
    var self = this;
    self.readonly = false;
    // Lists of fruit names and Vegetable objects
    self.fruitNames = ['Apple', 'Banana', 'Orange'];
    self.roFruitNames = angular.copy(self.fruitNames);
    self.tags = [];
    self.vegObjs = [
      {
        'name' : 'Broccoli',
        'type' : 'Brassica'
      },
      {
        'name' : 'Cabbage',
        'type' : 'Brassica'
      },
      {
        'name' : 'Carrot',
        'type' : 'Umbelliferous'
      }
    ];
    self.newVeg = function(chip) {
      return {
        name: chip,
        type: 'unknown'
      };
    };
  }
  

  
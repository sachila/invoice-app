var clientRegistry = require("../../Core/ClientRegistry.js")
var configManager = require("../../Core/ConfigurationManager.js")

function execute(commandData){
	var toClient = clientRegistry.getClient(commandData.data.from);

	var configData = configManager.get(commandData.data.class);

	if (!configData)
		commandData = {};

	if (toClient){ //client is online
		var sendData = {command:"globalconfigrecieved", data:configData}
		sendData.class = "config";

		console.log(sendData);
		toClient.invokeCommand("agentCommand", sendData);
	}
}


exports.execute = execute
var clientRegistry = require("../../Core/ClientRegistry.js")
var configManager = require("../../Core/ConfigurationManager.js")
var eventManager = require("../../Control/EventManager.js")

function execute(commandData){

	var toClient = clientRegistry.getClient(commandData.data.from);

	var sectionId = parseInt(commandData.data.section.split(' ')) - 1;
	var configData = configManager.getFull(commandData.data.name);
	configData.data.config[sectionId] = commandData.data.data;

	console.log(JSON.stringify(configData))

	configManager.save(configData);

	console.log("Saving Global Config...")
	var sendObject = {name:commandData.data.name, data:{}, from:commandData.data.from};
	//console.log(sendObject);
	eventManager.tasks.trigger("globalConfigChanged.StoreConfig", sendObject ,clientRegistry);


}


exports.execute = execute
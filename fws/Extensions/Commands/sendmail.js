var clientRegistry = require("../../Core/ClientRegistry.js")

function execute(commandData) {

    var toClient = clientRegistry.getClient(commandData.data.from);

    if (toClient) { //client is online
        var mailExt = toClient.getExtension("Email");
        if (mailExt) {

            var sendmail = mailExt.mail(commandData.to, commandData.subject, commandData.body);

            toClient.invokeCommand("mailsent", {});
        }
    } else { //client is offline
    }
}


exports.execute = execute
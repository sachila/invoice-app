var clientRegistry = require("../../Core/ClientRegistry.js")
var configManager = require("../../Core/ConfigurationManager.js")

function execute(commandData){
	var toClient = clientRegistry.getClient(commandData.data.from);

	if (toClient){ //client is online
		var data = [];

		var resources = clientRegistry.getClientsByResource("server");
		var globalconfigs = configManager.getAll();

		var servers = {group:"servers", caption:"Servers", subitems:[]}
		
		for (rIndex in resources){
			servers.subitems.push({caption:resources[rIndex].data.userName, displayType:"agent", displayId:resources[rIndex].data.userName, subitems:[]})
		}
		data.push(servers)

		var configs = {group:"config", caption: "Configuration", subitems:[]}
		
		for (cIndex in globalconfigs){
			configs.subitems.push({name: globalconfigs[cIndex].class, displayType:"config", displayId:globalconfigs[cIndex].class, caption:globalconfigs[cIndex].name, description:globalconfigs[cIndex].name});
		}
		data.push(configs);

		data.push({group:"tenant", caption:"Tenant", subitems:[{caption:"DuoV6 Tenant", displayType:"tenant", displayId:"duov6tenant", description:"..."}]});
		
		/*
		var toClient = clientRegistry.getClient(commandData.data.from);

		if (toClient){ //client is online

			var data = [
				{group:"tenant", caption:"Tenant", subitems:[{caption:"DuoV6 Tenant", displayType:"tenant", displayId:"duov6tenant", description:"..."}]},
				{group:"servers", caption:"Servers", subitems:[{caption:"duov6server", displayType:"agent", displayId:"duov6server@duov6.com", subitems:[
						{name : "objectstore1", caption: "objectstore1@duov6.com", displayId:"objectstore1@duov6.com", displayType:"agent", description:"..."},
						{name : "elastic1", caption: "elastic1@duov6.com", displayId:"elastic1@duov6.com", displayType:"agent", description:"..."},
						{name : "elastic2", caption: "elastic2@duov6.com", displayId:"elastic2@duov6.com", displayType:"agent", description:"..."},
						{name : "elastic3", caption: "elastic3@duov6.com", displayId:"elastic3@duov6.com", displayType:"agent", description:"..."},
						{name : "couchbase1", caption: "couchbase1@duov6.com", displayId:"couchbase1@duov6.com", displayType:"agent", description:"..."},
						{name : "redis1", caption: "redis1@duov6.com", displayId:"redis1@duov6.com", displayType:"agent", description:"..."},
						{name : "duoauth1", caption: "duoauth1@duov6.com", displayId:"duoauth1@duov6.com", displayType:"agent", description:"..."}
					]}]},
				{group:"config", caption: "Configuration", subitems:[{name: "tenantconfig", displayType:"config", displayId:"tenantConfig", caption:"Tenant Configuration", description:"This is the tenant configuration"}
				]}];
		}*/


		//console.log(data);

		toClient.invokeCommand("onDockerMap", data);
	}
}


exports.execute = execute

var clientRegistry = require("../../Core/ClientRegistry.js")

function execute(commandData){
	var toClient = clientRegistry.getClient(commandData.data.requestor);
	toClient.invokeCommand("resourceReleased", {name:commandData.data.id});
}


exports.execute = execute
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'johnbirdmangtalk@gmail.com',
        pass: 'johnbirdman123'
    }
});


function Extension() {

    function sendMail(to, subject, body){

        //CHANGE MAILING DETAILS HERE
        var mailOptions = {
            from: 'Duo Software Mail Service <johnbirdmangtalk@gmail.com>',
            to: to,
            subject: subject,
            text: body,
            //html: '<b>Hello world ✔</b>' // html body
        };

        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email Message sent successfully from Duo Software');
            }
        });
    }


    var onRecieve = function() {};

    return {
        start: function(parameters) {
            start(parameters)
        },
        stop: function() {
            stop()
        },
        mail: function(to, subject, body) {
            sendMail(to, subject, body);
        },
        onRecieveContent: function(func) {
            onRecieve = func;
        },
        saveContent: function(content, parameters) {
            saveContent(content, parameters)
        }
    }
};

exports.newInstance = function() {
    var extension = new Extension();
    return extension;
};

exports.run = exports = function() {
    Extension();
}
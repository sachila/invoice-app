var logger = require("./Logger.js");

var onlineClients = [];

// socket, data

function addClient(socket, clientData, authData){
	logger.log("New client registered : " + clientData.userName)
	var newClient = {socket: socket, data: clientData, authData: authData, intInstances: {}};
	onlineClients.push(newClient);

	return newClient;
/*
	users = [];
	for (index in onlineClients)
		users.push(onlineClients[index].data.userName)

	for (index in onlineClients)
		onlineClients[index].socket.emit('usernames', users);
*/
}

function removeClient (socket, clientData){
	var removeIndex = -1;
	for (index in onlineClients)
	if (onlineClients[index].socket === socket){
		removeIndex = index;
		break;
	}

	var removeClient;

	logger.log ("Remove Before : " + onlineClients.length);
	if (removeIndex !=-1){

		removeClient = onlineClients[removeIndex];

		onlineClients.splice(removeIndex,1);
	}

	logger.log ("Remove After : " + onlineClients.length);

	return removeClient;
}

function getAllClients(authenticationData){
	return onlineClients;
}	


function getClient(username){

	for (index in onlineClients)
	if (onlineClients[index].data.userName === username){
		return {
			//name, type, data, token
			invokeCommand : function(name,data,from){
				
				var sendClients = [];
				for (sIndex in onlineClients){

					if (onlineClients[sIndex].data.userName === username){
						sendClients.push (onlineClients[sIndex]);
						//break;

					}
				}
				if (!from)
					from ="SERVER"
				
				for (cIndex in sendClients){
					sendClients[cIndex].socket.send({name:name, type:"command", data:data, from:from});
				}

			},
			triggerEvent : function(name,data){
				
				var sendClients = [];
				for (sIndex in onlineClients){

					if (onlineClients[sIndex].data.userName === username){
						sendClients.push (onlineClients[sIndex]);
						//break;

					}
				}

				logger.log("Eventing...")

				for (cIndex in sendClients)
					sendClients[cIndex].socket.send({name:name, type:"event", data:data});

			},
			getExtension: function(extName){
				for (eIndex in onlineClients[sIndex].intInstances)
					if (onlineClients[sIndex].intInstances[eIndex].name === extName)
						return onlineClients[sIndex].intInstances[eIndex];
			}
		}	
	}

	return null;
}

function getResource(routingInfo){

	var availableClients = [];

	for(cIndex in onlineClients)
		if (onlineClients[cIndex].data.userName != routingInfo.requestor)
			availableClients.push(onlineClients[cIndex]);

	var currentIndex = random(0, availableClients.length);
	logger.log("CurrentIndex : " + currentIndex);

	var currentClient = availableClients[currentIndex];

	logger.log("Resource Acquired : " + currentClient.data.userName);
	return currentClient; //getClient(currentClient.data.userName)	
	
}


function random (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

function getClientsByResource(resource){
	outData = [];

	for (index in onlineClients)
	if (onlineClients[index].data.resourceClass === resource){
		outData.push(onlineClients[index])
	}
	return outData;
}

exports.addClient = addClient
exports.removeClient = removeClient
exports.getAllClients = getAllClients
exports.getClient = getClient
exports.getResource = getResource
exports.getClientsByResource = getClientsByResource
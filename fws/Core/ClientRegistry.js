var logger = require("./Logger.js");
var localRegistry = require("./LocalClientRegistry.js");


function addClient(socket, clientData, authData){
	localRegistry.addClient(socket, clientData, authData);
}

function removeClient (socket, clientData){
	return localRegistry.removeClient (socket, clientData);
}

function getAllClients(authenticationData){
	return localRegistry.getAllClients(authenticationData);
}	


function getClient(username){
	return localRegistry.getClient(username);
}

function getResource(routingInfo){
	return localRegistry.getResource(routingInfo);
}

function getClientsByResource(resource){
	return localRegistry.getClientsByResource(resource);
}


exports.addClient = addClient
exports.removeClient = removeClient
exports.getAllClients = getAllClients
exports.getClient = getClient
exports.getResource = getResource
exports.getClientsByResource = getClientsByResource
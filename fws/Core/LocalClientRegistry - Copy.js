var logger = require("./Logger.js");

var onlineClients = [];

// socket, data

function addClient(socket, clientData, authData){
	logger.log("New client registered : " + clientData.userName)
	onlineClients.push({socket: socket, data: clientData});


/*
	users = [];
	for (index in onlineClients)
		users.push(onlineClients[index].data.userName)

	for (index in onlineClients)
		onlineClients[index].socket.emit('usernames', users);
*/
}

function removeClient (socket, clientData){
	var removeIndex = -1;
	for (index in onlineClients)
	if (onlineClients[index].socket === socket){
		removeIndex = index;
		break;
	}

	var removeClient;

	if (removeIndex !=-1){

		removeClient = onlineClients[removeIndex];

		onlineClients.splice(removeIndex,1);
	}

	return removeClient;
}

function getAllClients(authenticationData){
	return onlineClients;
}	


function getClient(username){

	for (index in onlineClients)
	if (onlineClients[index].data.userName === username){
		return {
			//name, type, data, token
			invokeCommand : function(name,data,from){
				
				var sendClient;
				for (sIndex in onlineClients){

					if (onlineClients[sIndex].data.userName === username){
						sendClient = onlineClients[sIndex];
						break;

					}
				}
				if (!from)
					from ="SERVER"
				
				sendClient.socket.send({name:name, type:"command", data:data, from:from});

			},
			triggerEvent : function(name,data){
				
				var sendClient;
				for (sIndex in onlineClients){

					if (onlineClients[sIndex].data.userName === username){
						sendClient = onlineClients[sIndex];
						break;

					}
				}

				logger.log("Eventing...")
				sendClient.socket.send({name:name, type:"event", data:data});

			},
		}	
	}

	return null;
}

function getResource(routingInfo){

	var availableClients = [];

	for(cIndex in onlineClients)
		if (onlineClients[cIndex].data.userName != routingInfo.requestor)
			availableClients.push(onlineClients[cIndex]);

	var currentIndex = random(0, availableClients.length);
	logger.log("CurrentIndex : " + currentIndex);

	var currentClient = availableClients[currentIndex];

	logger.log("Resource Acquired : " + currentClient.data.userName);
	return currentClient; //getClient(currentClient.data.userName)	
	
}


function random (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

function getClientsByResource(resource){
	outData = [];

	for (index in onlineClients)
	if (onlineClients[index].data.resourceClass === resource){
		outData.push(onlineClients[index])
	}
	return outData;
}

exports.addClient = addClient
exports.removeClient = removeClient
exports.getAllClients = getAllClients
exports.getClient = getClient
exports.getResource = getResource
exports.getClientsByResource = getClientsByResource
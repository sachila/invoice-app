
var fs=require('fs');
var logger = require("../Core/Logger.js");

var allConfigs = [];



function get(cls){	
	for (index in allConfigs)
		if (allConfigs[index].data.class == cls)
			return allConfigs[index].data;
}

function getFull(cls){	
	for (index in allConfigs)
	{
		if (allConfigs[index].data.class == cls){
			return allConfigs[index];
		}
	}
}

function getAll(){
	var modifiedConfigs = [];

	for (index in allConfigs)
		modifiedConfigs.push(allConfigs[index].data)

	return modifiedConfigs;
}


function initialize(){
	var dir='./Config/';
	var data={};

	fs.readdir(dir,function(err,files){
	    if (err) throw err;
	    var c=0;
	    files.forEach(function(file){
	        c++;
	        fs.readFile(dir+file,'utf-8',function(err,html){
	            if (err) throw err;
	            allConfigs.push({fileName:file, data:JSON.parse(html)});
	            if (0===--c)
	                logger.log ("Global configuration load successful..");
	        });
	    });

	});

	
}


function save(data){
	fs.writeFile('./Config/' + data.fileName, JSON.stringify(data.data), function(err) {
	    if(err)
	        return console.log(err);

	    console.log("The file was saved!");

		for (index in allConfigs)
			if (allConfigs[index].fileName == data.fileName)
				allConfigs[index] = data;

	}); 
}

exports.initialize = initialize;
exports.get = get;
exports.getFull = getFull;
exports.getAll = getAll;
exports.save = save;